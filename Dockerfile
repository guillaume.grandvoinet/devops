FROM php:8.3-apache
RUN docker-php-ext-install pdo_mysql
ADD src/ /var/www/html
EXPOSE 80